const axios = require('axios');
const htmlparser2 = require("htmlparser2");

let inCity = false;
let inDate = false;
let inTemperature = false;
let countRows = false;
let done = false;
let rowCount1 = 0;
let city = "";

const parser = new htmlparser2.Parser(
    {
        onopentag(name, attribs) {
            // if (name === "title") {
            //     inTitle = true;
            // }
            if (name === "dd") {
                rowCount1++;
                inDate = true;
                inTemperature = true;
            }
            if (name == "h1") {
                inCity = true;
            }
        },
        onattribute(name, value) {
            // if (name === "class" && value === "table-responsive table-basic hide-mobile") {
            //     countRows = true;
            // }
        },
        ontext(text) {
            if (inCity) {
                city += text;
            }
            if (inDate && rowCount1 == 2) {
                console.log("Date/Time: " + text + "\n");
            }
            inDate = false;
            if (inTemperature && rowCount1 == 7) {
                console.log("Temperature: " + text + "\n");
            }
            inTemperature = false;
        },
        onclosetag(tagname) {
            if (tagname === "h1") {
                console.log("City: " + city);
                inCity = false;
            }
            if (tagname === "html") {
                console.log("That's it!");
            }
            // if (tagname === "table" && countRows && !done) {
            //     console.log("There are " + (rowCount - 1) + " computer labs.");
            //     done = true;
            // }
        }
    },
    { decodeEntities: true }
);

if (process.argv.length > 2) {
    axios.get(process.argv[2])
        .then(response => {
            parser.write(response.data);
            parser.end();
        })
        .catch(error => {
            console.log("Could not fetch page.");
        });
} else {
    console.log("Missing URL argument");
}